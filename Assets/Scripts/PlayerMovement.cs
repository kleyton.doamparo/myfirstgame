﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class PlayerMovement : MonoBehaviour
{
    public PlayerMovement myScript;
    
    public float weight=1.0f;
    public float speed = 0.5f; //la velocidad depende del peso
    
    public SpriteRenderer mySpriteRenderer;

    public Sprite spriteFront;
    public Sprite spriteBack;
    public Sprite spriteRight;
    public Sprite spriteLeft;


    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 120;

        try{
        mySpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        }catch(NullReferenceException ex){
            Debug.LogError("SpriteRenderer not found! (Harl!)");
        }

        myScript=this.gameObject.GetComponent<PlayerMovement>();
        weight=myScript.weight;
    }

    // Update is called once per frame
    void Update()
    {
        

        if(Input.GetKeyUp(KeyCode.W)){
            transform.position += new Vector3(0,this.speed,0);
            mySpriteRenderer.sprite=spriteBack;
        }
        if(Input.GetKeyUp(KeyCode.S)){
            transform.position -= new Vector3(0,this.speed,0);
            mySpriteRenderer.sprite=spriteFront;
        }
        if(Input.GetKeyUp(KeyCode.A)){
            transform.position -= new Vector3(this.speed,0,0);
            mySpriteRenderer.sprite=spriteLeft;

        }
        if(Input.GetKeyUp(KeyCode.D)){
            transform.position += new Vector3(this.speed,0,0);
            mySpriteRenderer.sprite=spriteRight;

        }if(Input.GetKeyUp(KeyCode.Space)){
            if(weight!=2.0f){
                speed*=(speed/weight);
                transform.localScale=new Vector3(2,2,2);
                weight=2.0f;//Aumenta de peso y reduce velocidad
                StartCoroutine(countDown());//Se vuelve pequeño en 5 segundos
            }              
        }
    
        var displacement = new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
        //transform.position += displacement*speed*Time.deltaTime;

        if(displacement.x>0) mySpriteRenderer.sprite=spriteRight;
        if(displacement.x<0) mySpriteRenderer.sprite=spriteLeft;
        if(displacement.y>0) mySpriteRenderer.sprite=spriteBack;
        if(displacement.y<0) mySpriteRenderer.sprite=spriteFront;
        
    }

    IEnumerator countDown(){
        yield return new WaitForSeconds(5.0f);
        transform.localScale=new Vector3(1,1,1);
        weight=1.0f;
        speed=0.5f;
    }


}
